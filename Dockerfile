FROM browsh/browsh

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends mosh

CMD ["/app/browsh"]
